﻿using Projekat2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Projekat2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AddEditClassroomWindow : Window
    {

        public Ucionica Ucionica = new Ucionica();
        public enum ClassroomWindowState { Addition, Editing}
        private ClassroomWindowState state;
        public ICollectionView CollectionView;
        public AddEditClassroomWindow(Ucionica ucionica, ClassroomWindowState classroomWindowState = ClassroomWindowState.Addition)
        {
            InitializeComponent();
            Ucionica = ucionica;
            state = classroomWindowState;
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ucionice);
            ClassroomTypeComboBox.Items.Add("BezRacunara");
            ClassroomTypeComboBox.Items.Add("SaRacunarima");
            DataContext = Ucionica;
        }

        private void SaveClassroomButton_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            if (state == ClassroomWindowState.Addition)
            {
                Ucionica.IdUcionice = ModelHelper.GetInstance().Ucionice.Count() + 1;
                string ClassroomType = (string)ClassroomTypeComboBox.SelectedItem;
                TipUcionice tipUcionice;
                Enum.TryParse(ClassroomType, out tipUcionice);
                Ucionica.TipUcionice = tipUcionice;
                ModelHelper.GetInstance().Ucionice.Add(Ucionica);
                ModelHelper.AddClassroom(Ucionica);

            }else if(state == ClassroomWindowState.Editing)
            {
                if (Ucionica.IdUcionice != 0)
                {
                    string ClassroomType = (string)ClassroomTypeComboBox.SelectedItem;
                    TipUcionice tipUcionice;
                    Enum.TryParse(ClassroomType, out tipUcionice);
                    Ucionica.TipUcionice = tipUcionice;
                    ModelHelper.EditClassroom(Ucionica);
                    CollectionView.Refresh();
                }    
            }
            this.Close();
        }

        private void DeleteClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            ModelHelper.DeleteClassroom(Ucionica);
            ModelHelper.GetInstance().Ucionice.Remove(Ucionica);
        }
    }
}
