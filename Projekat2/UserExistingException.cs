﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat2
{
    public class UserExistingException : Exception
    {
        public UserExistingException(String message) : base(message) { }

    }
}
