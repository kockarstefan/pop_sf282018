﻿using Projekat2.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public Korisnik k { get; set; }
        public ICollectionView CollectionView;
        public ICollectionView SearchedCollectionView;
        public MainWindow(Korisnik k)
        {
            InitializeComponent();
            if (k.TipKorisnika.ToString() == "GUEST")
            {
                ModelHelper.GetAsistentsAndProfesors();
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().GuestUsersList);
                UsersGrid.ItemsSource = CollectionView;
                UsersGrid.IsSynchronizedWithCurrentItem = true;
                CollectionView.Refresh();
                SearchFilterCb.IsEnabled = false;
                SearchFilter.IsEnabled = false;
                AddUserButton.IsEnabled = false;
                EditUserButton.IsEnabled = false;
                DeleteUserButton.IsEnabled = false;
                ShowClassroomsButton.IsEnabled = false;
                ShowPeriodsButton.IsEnabled = false;
                this.Title = "Guest";
            }
            else
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Korisnici);
                UsersGrid.ItemsSource = CollectionView;
                UsersGrid.IsSynchronizedWithCurrentItem = true;

                SearchFilterCb.Items.Add("name");
                SearchFilterCb.Items.Add("lastname");
                SearchFilterCb.Items.Add("email");
                SearchFilterCb.Items.Add("typeOfUser");
                SearchFilterCb.Items.Add("username");
                SearchFilterCb.Items.Add("active");
                this.Title = "Welcome " + k.KorisnickoIme;
            }
        }


        private void AddUserButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik noviKorisnik = new Korisnik();
            AddEditUserWindow addEditUserWindow = new AddEditUserWindow(noviKorisnik);
            addEditUserWindow.ShowDialog();
            CollectionView.Refresh();
        }

        private void EditUserButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik SelektovaniKorisnik = CollectionView.CurrentItem as Korisnik;
            if (SelektovaniKorisnik != null)
            {
                Korisnik old = (Korisnik)SelektovaniKorisnik.Clone();
                AddEditUserWindow editUserWindow = new AddEditUserWindow(SelektovaniKorisnik, AddEditUserWindow.UserWindowState.Editing);
                if (editUserWindow.ShowDialog() != true)
                {
                    int index = ModelHelper.GetInstance().Korisnici.IndexOf(SelektovaniKorisnik);
                    ModelHelper.GetInstance().Korisnici[index] = old;
                    CollectionView.Refresh();
                }
            }
        }
        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik SelektovaniKorisnik = CollectionView.CurrentItem as Korisnik;
            ModelHelper.DeleteUser(SelektovaniKorisnik);
            CollectionView.Refresh();
        }

        private void ShowClassroomsButton_Click(object sender, RoutedEventArgs e)
        {
            ClassroomsWindow window = new ClassroomsWindow();
            window.Show();
        }

        private void ShowBuildingsButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik loggedInUser = ModelHelper.GetInstance().GetKorisnikByKorIme(this.Title);
            BuildingsWindow window = new BuildingsWindow(loggedInUser);
            window.Show();
        }
        private void ShowPeriodsButton_Click(object sender, RoutedEventArgs e)
        {
            Korisnik loggedInUser = ModelHelper.GetInstance().GetKorisnikByKorIme(this.Title);
            TerminiWindow window = new TerminiWindow(loggedInUser);
            window.Show();
        }

        private void SearchFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String param1 = (string)SearchFilterCb.SelectedItem;
            String param2 = SearchFilter.Text;
            if (param2 == "")
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Korisnici);
                ModelHelper.GetInstance().FoundUsers.Clear();
                UsersGrid.ItemsSource = CollectionView;
                UsersGrid.IsSynchronizedWithCurrentItem = true;
            }
            else
            {

                ModelHelper.SearchUsersBy(param1, param2);
                SearchedCollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().FoundUsers);
                UsersGrid.ItemsSource = SearchedCollectionView;
                CollectionView.Refresh();

            }
        }
    }
}