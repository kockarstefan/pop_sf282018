﻿using Projekat2;
using Projekat2.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using static Projekat2.Model.Termin;

namespace Projekat2
{
    public class ModelHelper
    {

        public const string CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=master;Integrated Security=True";
        private ModelHelper() { }

        private static ModelHelper instance;

        public List<Korisnik> Korisnici = new List<Korisnik>();
        public List<Ustanova> Ustanove = new List<Ustanova>();
        public List<Ucionica> Ucionice = new List<Ucionica>();
        public List<Termin> Termini = new List<Termin>();

        public List<Korisnik> FoundUsers = new List<Korisnik>();
        public List<Ustanova> FoundBuildings = new List<Ustanova>();
        public List<Ucionica> FoundClassrooms = new List<Ucionica>();
        public List<Termin> FoundPeriods = new List<Termin>();

        public List<Korisnik> GuestUsersList = new List<Korisnik>();
        public List<Termin> ProfessorsPeriods = new List<Termin>(); 



        public static ModelHelper GetInstance()
        {
            if (instance == null)
            {
                instance = new ModelHelper();
            }

            return instance;
        }



        public void InitKorisnici()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand korisniciCommand = connection.CreateCommand();
                korisniciCommand.CommandText = @"SELECT * FROM Users WHERE active=1";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = korisniciCommand;
                daKorisnici.Fill(ds, "User");

                foreach (DataRow row in ds.Tables["User"].Rows)
                {
                    Korisnik k = new Korisnik();
                    
                    k.Ime = (string)row["name"];
                    k.Prezime = (string)row["lastname"];
                    k.Email = (string)row["email"];
                    k.KorisnickoIme = (string)row["username"];
                    TipKorisnika tipKorisnika;
                    Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                    k.TipKorisnika = tipKorisnika;
                    k.Active = (bool)row["active"];

                    ModelHelper.GetInstance().Korisnici.Add(k);

                }
            }
        }

        public static void AddUser(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO USERS (NAME, LASTNAME, EMAIL, USERNAME, PASSWORD, TYPEOFUSER, ACTIVE) VALUES (@Name, @Lastname, @Email, @Username, @Password, @TypeOfUser, @Active)";

                command.Parameters.Add(new SqlParameter("@Name", k.Ime));
                command.Parameters.Add(new SqlParameter("@Lastname", k.Prezime));
                command.Parameters.Add(new SqlParameter("@Email", k.Email));
                command.Parameters.Add(new SqlParameter("@Username", k.KorisnickoIme));
                command.Parameters.Add(new SqlParameter("@Password", k.Lozinka));
                command.Parameters.Add(new SqlParameter("@TypeOfUser", k.TipKorisnika.ToString()));
                command.Parameters.Add(new SqlParameter("@Active", (bool)k.Active));

                command.ExecuteNonQuery();
                
            }
        }

        public static void EditUser(Korisnik k)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                if (k.Id != 0)
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE USERS SET NAME=@Name, LASTNAME=@Lastname, EMAIL=@Email, USERNAME=@Username, PASSWORD=@Password, TYPEOFUSER=@TypeOfUser, ACTIVE=@Active  WHERE Username=@Username";

                    
                    command.Parameters.Add(new SqlParameter("@Name", k.Ime));
                    command.Parameters.Add(new SqlParameter("@Lastname", k.Prezime));
                    command.Parameters.Add(new SqlParameter("@Email", k.Email));
                    command.Parameters.Add(new SqlParameter("@Username", k.KorisnickoIme));
                    command.Parameters.Add(new SqlParameter("@Password", k.Lozinka));
                    command.Parameters.Add(new SqlParameter("@TypeOfUser", k.TipKorisnika.ToString()));
                    command.Parameters.Add(new SqlParameter("@Active", (bool)k.Active));

                    command.ExecuteNonQuery();
                }
            }
        }

        public void InitUstanove()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand ustanoveCommand = connection.CreateCommand();
                ustanoveCommand.CommandText = @"SELECT * FROM Buildings WHERE active=1";
                SqlDataAdapter daUstanove = new SqlDataAdapter();
                daUstanove.SelectCommand = ustanoveCommand;
                daUstanove.Fill(ds, "Building");

                foreach (DataRow row in ds.Tables["Building"].Rows)
                {
                    Ustanova u = new Ustanova();
                    u.Id = (int)row["Id"];
                    u.NazivUstanove = (string)row["name"];
                    u.LokacijaUstanove = (string)row["location"];
                    u.Upotreba = (bool)row["active"];

                    ModelHelper.GetInstance().Ustanove.Add(u);

                }
            }
        }

        public static void AddUstanova(Ustanova u)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO BUILDINGS (NAME, LOCATION, ACTIVE) VALUES (@Name, @Location, @Active)";

                command.Parameters.Add(new SqlParameter("@Name", u.NazivUstanove));
                command.Parameters.Add(new SqlParameter("@Location", u.LokacijaUstanove));
                command.Parameters.Add(new SqlParameter("@Active", (bool)u.Upotreba));

                command.ExecuteNonQuery();
            }
        }

        public static void EditUstanova(Ustanova u)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                if (u.Id != 0)
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE BUILDINGS SET NAME=@Name, LOCATION=@Location, ACTIVE=@Active  WHERE ID=@Id";

                    command.Parameters.Add(new SqlParameter("@Id", u.Id));
                    command.Parameters.Add(new SqlParameter("@Name", u.NazivUstanove));
                    command.Parameters.Add(new SqlParameter("@Location", u.LokacijaUstanove));
                    command.Parameters.Add(new SqlParameter("@Active", (bool)u.Upotreba));

                    command.ExecuteNonQuery();
                }
            }
        }

        public void initUcionice()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand ucioniceCommand = connection.CreateCommand();
                ucioniceCommand.CommandText = @"SELECT * FROM Classrooms WHERE active=1";
                SqlDataAdapter daUcionice = new SqlDataAdapter();
                daUcionice.SelectCommand = ucioniceCommand;
                daUcionice.Fill(ds, "Classroom");

                foreach (DataRow row in ds.Tables["Classroom"].Rows)
                {
                    Ucionica u = new Ucionica();
                    u.IdUcionice = (int)row["Id"];
                    u.BrojUcionice = (int)row["classnum"];
                    u.BrojMesta = (int)row["numOfSeats"];
                    TipUcionice tipUcionice;
                    Enum.TryParse((string)row["classType"], out tipUcionice);
                    u.TipUcionice = tipUcionice;
                    u.Upotreba = (bool)row["active"];

                    ModelHelper.GetInstance().Ucionice.Add(u);

                }
            }
        }

        public static void AddClassroom(Ucionica u)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO CLASSROOMS (CLASSNUM, NUMOFSEATS, CLASSTYPE, ACTIVE) VALUES (@Classnum, @NumOfSeats,@ClassType, @Active)";

                command.Parameters.Add(new SqlParameter("@Classnum", u.BrojUcionice));
                command.Parameters.Add(new SqlParameter("@NumOfSeats", u.BrojMesta));
                command.Parameters.Add(new SqlParameter("@ClassType", u.TipUcionice.ToString()));
                command.Parameters.Add(new SqlParameter("@Active", (bool)u.Upotreba));

                command.ExecuteNonQuery();
            }
        }

        public static void EditClassroom(Ucionica u)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                if (u.IdUcionice != 0)
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE CLASSROOMS SET CLASSNUM=@Classnum, NUMOFSEATS=@NumOfSeats, CLASSTYPE=@ClassType, ACTIVE=@Active  WHERE ID=@Id";

                    command.Parameters.Add(new SqlParameter("@Id", u.IdUcionice));
                    command.Parameters.Add(new SqlParameter("@Classnum", u.BrojUcionice));
                    command.Parameters.Add(new SqlParameter("@NumOfSeats", u.BrojMesta));
                    command.Parameters.Add(new SqlParameter("@ClassType", u.TipUcionice.ToString()));
                    command.Parameters.Add(new SqlParameter("@Active", (bool)u.Upotreba));

                    command.ExecuteNonQuery();
                }
            }
        }

        public void initTermini()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand terminiCommand = connection.CreateCommand();
                terminiCommand.CommandText = @"SELECT * FROM Termini WHERE Active=1";
                SqlDataAdapter daTermini = new SqlDataAdapter();
                daTermini.SelectCommand = terminiCommand;
                daTermini.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    Termin t = new Termin();
                    t.VremeZauzeca = (string)row["VremeZauzeca"];
                    t.Datum = (string)row["Datum"];
                    TipNastave tipNastave;
                    Enum.TryParse((string)row["Type"], out tipNastave);
                    t.tipNastave = tipNastave;
                    t.Active = (bool)row["active"];
                    t.korisnik = GetKorisnik((int)row["KorisnikId"]);
                    int id = (int)row["KorisnikId"];
                    t.korisnik.Id = id;

                    ModelHelper.GetInstance().Termini.Add(t);

                }
            }
        }

        public static void AddTermin(Termin t)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                conn.Open();

                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"INSERT INTO TERMINI (VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE) VALUES (@VremeZauzeca, @Datum, @Type,@KorisnikId, @Active)";

                command.Parameters.Add(new SqlParameter("@VremeZauzeca", t.VremeZauzeca));
                command.Parameters.Add(new SqlParameter("@Datum", t.Datum));
                command.Parameters.Add(new SqlParameter("@Type", t.tipNastave.ToString()));
                command.Parameters.Add(new SqlParameter("@KorisnikId", t.korisnik.Id));
                command.Parameters.Add(new SqlParameter("@Active", (bool)t.Active));

                command.ExecuteNonQuery();
            }
        }

        public static void EditTermin(Termin t)
        {
            using (SqlConnection conn = new SqlConnection(CONNECTION_STRING))
            {
                if (t.Id != 0)
                {
                    conn.Open();

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE TERMINI SET VREMEZAUZECA=@VremeZauzeca, DATUM=@Datum, TYPE=@Type, KORISNIKID=@KorisnikId, ACTIVE=@Active WHERE ID=@Id";

                    command.Parameters.Add(new SqlParameter("@Id", t.Id));
                    command.Parameters.Add(new SqlParameter("@VremeZauzeca", t.VremeZauzeca));
                    command.Parameters.Add(new SqlParameter("@Datum", t.Datum));
                    command.Parameters.Add(new SqlParameter("@Type", t.tipNastave.ToString()));
                    foreach (var user in ModelHelper.GetInstance().Korisnici)
                    {
                        if (user.Id == t.korisnik.Id)
                        {
                            command.Parameters.Add(new SqlParameter("@KorisnikId", user.Id));
                        }
                        else
                        {
                            break;
                        }
                    }
                    command.Parameters.Add(new SqlParameter("@Active", (bool)t.Active));

                    command.ExecuteNonQuery();
                }
            }
        }

        public static void GetAsistentsAndProfesors()
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand korisniciCommand = connection.CreateCommand();
                korisniciCommand.CommandText = @"SELECT * FROM Users WHERE active=1 and (typeOfUser='PROFESOR' or typeOfUser='ASISTENT')";
                SqlDataAdapter daKorisnici = new SqlDataAdapter();
                daKorisnici.SelectCommand = korisniciCommand;
                daKorisnici.Fill(ds, "User");

                foreach (DataRow row in ds.Tables["User"].Rows)
                {
                    Korisnik k = new Korisnik();

                    k.Ime = (string)row["name"];
                    k.Prezime = (string)row["lastname"];
                    k.Email = (string)row["email"];
                    k.KorisnickoIme = (string)row["username"];
                    TipKorisnika tipKorisnika;
                    Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                    k.TipKorisnika = tipKorisnika;
                    k.Active = (bool)row["active"];

                    ModelHelper.GetInstance().GuestUsersList.Add(k);

                }
            }
        }


        public void WriteUsersToFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream outputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Create, FileAccess.Write);

            formatter.Serialize(outputStream, Korisnici);
            outputStream.Close();
        }

        public void ReadUsersFromFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream inputStream = new FileStream(@"../../Resources/korisnici.bin", FileMode.Open, FileAccess.Read);

            Korisnici = (List<Korisnik>)formatter.Deserialize(inputStream);
            inputStream.Close();
        }

        public void WriteInstitutionsToFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream outputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Create, FileAccess.Write);

            formatter.Serialize(outputStream, Ustanove);
            outputStream.Close();
        }

        public void ReadInstitutionsFromFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream inputStream = new FileStream(@"../../Resources/ustanove.bin", FileMode.Open, FileAccess.Read);

            Ustanove = (List<Ustanova>)formatter.Deserialize(inputStream);
            inputStream.Close();
        }

        public void WriteClassroomsToFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream outputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Create, FileAccess.Write);

            formatter.Serialize(outputStream, Ucionice);
            outputStream.Close();
        }

        public void ReadClassromsFromFile()
        {
            IFormatter formatter = new BinaryFormatter();
            Stream inputStream = new FileStream(@"../../Resources/ucionice.bin", FileMode.Open, FileAccess.Read);

            Ucionice = (List<Ucionica>)formatter.Deserialize(inputStream);
            inputStream.Close();
        }

        public Korisnik GetKorisnik(int id)
        {
            Korisnik k = new Korisnik();
            foreach (var korisnik in Korisnici)
            {
                if (korisnik.Id == id)
                {
                    k = korisnik;
                }
            }
            return k;

        }





        public Ustanova GetUstanova(int id)
        {
            foreach (var ustanova in Ustanove)
            {
                if (ustanova.Id == id)
                {
                    return ustanova;
                }
            }
            return null;
        }

        public Ucionica GetUcionica(int id)
        {
            foreach (var ucionica in Ucionice)
            {
                if (ucionica.IdUcionice == id)
                {
                    return ucionica;
                }
            }
            return null;
        }

        public static void DeleteUser(Korisnik k)
        {
            k.Active = false;
        }

        public static void DeleteClassroom(Ucionica u)
        {
            u.Upotreba = false;
        }

        public static void DeleteUstanovu(Ustanova u)
        {
            u.Upotreba = false;
        }

        public static void DeletePeriod(Termin t)
        {
            t.Active = false;
        }

        public static void CheckKorisnik(Korisnik k)
        {
            int i = 0;
            for (; ModelHelper.GetInstance().Korisnici.Count() > i; i++)
            {
                if (ModelHelper.GetInstance().Korisnici.ElementAt(i).KorisnickoIme == k.KorisnickoIme)
                {
                    MessageBox.Show("Korisnik sa ovim korisnickim imenom vec postoji!");

                }
                else if (ModelHelper.GetInstance().Korisnici.ElementAt(i).Email == k.Email)
                {
                    MessageBox.Show("Korisnik sa ovim email-om vec postoji!");
                }
            }
            ModelHelper.GetInstance().Korisnici.Add(k);
        }



        public void CheckUstanova(Ustanova u)
        {
            int i = 0;
            for (; Ustanove.Count > i; i++)
            {
                if (Ustanove.ElementAt(i).NazivUstanove == u.NazivUstanove)
                {
                    throw new Exception("Ustanova sa ovim nazivom vec postoji!");
                }
                else if (Ustanove.ElementAt(i).Id == u.Id)
                {
                    throw new Exception("Ustanova sa ovim id-om vec postoji!");
                }
                else if (Ustanove.ElementAt(i).LokacijaUstanove == u.LokacijaUstanove)
                {
                    throw new Exception("Ustanova na ovoj lokaciji vec postoji!");
                }
            }
            Ustanove.Add(u);
        }

        public void CheckUcionica(Ucionica u)
        {
            int i = 0;
            for (; Ucionice.Count > i; i++)
            {
                if (Ucionice.ElementAt(i).BrojUcionice == u.BrojUcionice)
                {
                    throw new Exception("Ucionica sa ovim brojem vec postoji!");
                }
                else if (Ucionice.ElementAt(i).IdUcionice == u.IdUcionice)
                {
                    throw new Exception("Ucionica sa ovim id-om vec postoji!");
                }
            }
            Ucionice.Add(u);
        }

        public static void GetProfessorsPeriods(Korisnik k)
        {
            using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
            {
                connection.Open();

                DataSet ds = new DataSet();

                SqlCommand terminiCommand = connection.CreateCommand();
                terminiCommand.CommandText = @"SELECT * FROM Termini WHERE Active=1 and (KorisnikId like '%' + @Id + '%')";

                terminiCommand.Parameters.Add(new SqlParameter("@Id", k.Id));

                SqlDataAdapter daTermini = new SqlDataAdapter();
                daTermini.SelectCommand = terminiCommand;
                daTermini.Fill(ds, "Termin");

                foreach (DataRow row in ds.Tables["Termin"].Rows)
                {
                    Termin t = new Termin();
                    t.VremeZauzeca = (string)row["VremeZauzeca"];
                    t.Datum = (string)row["Datum"];
                    TipNastave tipNastave;
                    Enum.TryParse((string)row["Type"], out tipNastave);
                    t.tipNastave = tipNastave;
                    t.Active = (bool)row["active"];
                    t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                    int id = (int)row["KorisnikId"];
                    t.korisnik.Id = id;

                    ModelHelper.GetInstance().ProfessorsPeriods.Add(t);

                }
            }
        }



        public void UpdateKorisnik(Korisnik original, Korisnik copy)
        {
            int index = Korisnici.IndexOf(original);
            Korisnici[index] = copy;
        }

        public void UpdateUstanovu(Ustanova original, Ustanova copy)
        {
            int index = Ustanove.IndexOf(original);
            Ustanove[index] = copy;
        }

        public void UpdateUcionicu(Ucionica original, Ucionica copy)
        {
            int index = Ucionice.IndexOf(original);
            Ucionice[index] = copy;
        }

        public Korisnik GetKorisnikByKorIme(string korisnickoIme)
        {
            Korisnik VraceniKorisnik = new Korisnik();
            foreach (var korisnik in Korisnici)
            {
                if (korisnik.KorisnickoIme.Contains(korisnickoIme))
                {
                    VraceniKorisnik = korisnik;
                }
            }
            return VraceniKorisnik;
        }

        public void SortUsersById()
        {
            Korisnici.OrderBy(u => u.Id);

        }
        public void SortUsersByEmail()
        {
            Korisnici.OrderBy(u => u.Email);
        }


        public static bool Login(string username, string password)
        {
            bool LoginSuccessful = true;
            try
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand loginCommand = connection.CreateCommand();
                    loginCommand.CommandText = @"SELECT * FROM Users WHERE USERNAME=@Username AND PASSWORD=@Password";

                    loginCommand.Parameters.Add(new SqlParameter("@Username", username));
                    loginCommand.Parameters.Add(new SqlParameter("@Password", password));

                    loginCommand.ExecuteNonQuery();

                    SqlDataAdapter daLogin = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daLogin.SelectCommand = loginCommand;
                    daLogin.Fill(ds);

                    
                    int count = ds.Tables[0].Rows.Count;
                    if (count == 1)
                    {
                        MessageBox.Show("Login successful!");
                        LoginSuccessful = true;
                        return LoginSuccessful;
                    }
                    else
                    {
                        MessageBox.Show("Invalid username or password");
                        LoginSuccessful = false;
                        return LoginSuccessful;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return LoginSuccessful;
        }

      
        public static void CheckSearchedUsers(Korisnik k)
        {
            if (ModelHelper.GetInstance().FoundUsers.Count() == 0)
            {
                ModelHelper.GetInstance().FoundUsers.Add(k);
            }

        }




        public static void SearchUsersBy(string param1, string param2)
        {
            if (param1 == "name")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();



                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE name LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];

                        ModelHelper.GetInstance().FoundUsers.Add(k);

                    }

                }
            }
            else if (param1 == "lastname")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE lastname LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];

                        ModelHelper.GetInstance().FoundUsers.Add(k);
                    }

                }
            }
            else if (param1 == "typeOfUser")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE typeOfUser LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];

                        ModelHelper.GetInstance().FoundUsers.Add(k);
                    }
                }
            }
            else if (param1 == "email")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE email LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundUsers.Add(k);
                    }
                }
            }
            else if (param1 == "username")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE username LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundUsers.Add(k);
                    }
                }
            }
            else if (param1 == "active")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    SqlCommand sql = connection.CreateCommand();
                    sql.CommandText = @"SELECT name, lastname, email, username, typeofuser, active FROM USERS WHERE active LIKE '%' + @Param2 + '%'";

                    sql.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daSearch = new SqlDataAdapter();
                    DataSet ds = new DataSet();
                    daSearch.SelectCommand = sql;
                    daSearch.Fill(ds, "FoundUsers");

                    foreach (DataRow row in ds.Tables["FoundUsers"].Rows)
                    {
                        Korisnik k = new Korisnik();
                        k.Ime = (string)row["name"];
                        k.Prezime = (string)row["lastname"];
                        k.Email = (string)row["email"];
                        k.KorisnickoIme = (string)row["username"];
                        TipKorisnika tipKorisnika;
                        Enum.TryParse((string)row["typeOfUser"], out tipKorisnika);
                        k.TipKorisnika = tipKorisnika;
                        k.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundUsers.Add(k);

                    }
                }
            }
        }

        public static void SearchClassroomsBy(string param1, string param2)
        {
            if (param1 == "classnumber")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand ucioniceCommand = connection.CreateCommand();
                    ucioniceCommand.CommandText = @"SELECT CLASSNUM, NUMOFSEATS, CLASSTYPE, ACTIVE FROM Classrooms WHERE classnum LIKE '%'+ @Param2 +'%'";

                    ucioniceCommand.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daUcionice = new SqlDataAdapter();
                    daUcionice.SelectCommand = ucioniceCommand;
                    daUcionice.Fill(ds, "FoundClassroom");

                    foreach (DataRow row in ds.Tables["FoundClassroom"].Rows)
                    {
                        Ucionica u = new Ucionica();
                        u.BrojUcionice = (int)row["classnum"];
                        u.BrojMesta = (int)row["numOfSeats"];
                        TipUcionice tipUcionice;
                        Enum.TryParse((string)row["classType"], out tipUcionice);
                        u.TipUcionice = tipUcionice;
                        u.Upotreba = (bool)row["active"];

                        ModelHelper.GetInstance().FoundClassrooms.Add(u);
                    }
                }

            }
            else if (param1 == "numOfSeats")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand ucioniceCommand = connection.CreateCommand();
                    ucioniceCommand.CommandText = @"SELECT CLASSNUM, NUMOFSEATS, CLASSTYPE, ACTIVE FROM Classrooms WHERE numofseats LIKE '%'+ @Param2 +'%'";
                    ucioniceCommand.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daUcionice = new SqlDataAdapter();
                    daUcionice.SelectCommand = ucioniceCommand;
                    daUcionice.Fill(ds, "FoundClassroom");

                    foreach (DataRow row in ds.Tables["FoundClassroom"].Rows)
                    {
                        Ucionica u = new Ucionica();
                        u.BrojUcionice = (int)row["classnum"];
                        u.BrojMesta = (int)row["numOfSeats"];
                        TipUcionice tipUcionice;
                        Enum.TryParse((string)row["classType"], out tipUcionice);
                        u.TipUcionice = tipUcionice;
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundClassrooms.Add(u);

                    }
                }
            }
            else if (param1 == "classtype")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand ucioniceCommand = connection.CreateCommand();
                    ucioniceCommand.CommandText = @"SELECT CLASSNUM, NUMOFSEATS, CLASSTYPE, ACTIVE FROM Classrooms WHERE classtype LIKE '%'+ @Param2 +'%'";
                    ucioniceCommand.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daUcionice = new SqlDataAdapter();
                    daUcionice.SelectCommand = ucioniceCommand;
                    daUcionice.Fill(ds, "FoundClassroom");

                    foreach (DataRow row in ds.Tables["FoundClassroom"].Rows)
                    {
                        Ucionica u = new Ucionica();
                        u.BrojUcionice = (int)row["classnum"];
                        u.BrojMesta = (int)row["numOfSeats"];
                        TipUcionice tipUcionice;
                        Enum.TryParse((string)row["classType"], out tipUcionice);
                        u.TipUcionice = tipUcionice;
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundClassrooms.Add(u);
                    }
                }
            }
            else if (param1 == "active")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand ucioniceCommand = connection.CreateCommand();
                    ucioniceCommand.CommandText = @"SELECT CLASSNUM, NUMOFSEATS, CLASSTYPE, ACTIVE FROM Classrooms WHERE active LIKE '%'+ @Param2 +'%'";
                    ucioniceCommand.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daUcionice = new SqlDataAdapter();
                    daUcionice.SelectCommand = ucioniceCommand;
                    daUcionice.Fill(ds, "FoundClassroom");

                    foreach (DataRow row in ds.Tables["FoundClassroom"].Rows)
                    {
                        Ucionica u = new Ucionica();
                        u.BrojUcionice = (int)row["classnum"];
                        u.BrojMesta = (int)row["numOfSeats"];
                        TipUcionice tipUcionice;
                        Enum.TryParse((string)row["classType"], out tipUcionice);
                        u.TipUcionice = tipUcionice;
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundClassrooms.Add(u);
                    }
                }
            }
        }

        public static void SearchBuildingsBy(string param1, string param2)
        {
            if (param1 == "name")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand buildingsCommand = connection.CreateCommand();
                    buildingsCommand.CommandText = @"SELECT NAME, LOCATION, ACTIVE FROM Buildings WHERE name LIKE '%'+ @Param2 +'%'";

                    buildingsCommand.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daBuildings = new SqlDataAdapter();
                    daBuildings.SelectCommand = buildingsCommand;
                    daBuildings.Fill(ds, "FoundBuilding");

                    foreach (DataRow row in ds.Tables["FoundBuilding"].Rows)
                    {
                        Ustanova u = new Ustanova();
                        u.NazivUstanove = (string)row["name"];
                        u.LokacijaUstanove = (string)row["location"];
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundBuildings.Add(u);
                    }
                }

            }
            else if (param1 == "location")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand buildingsCommand = connection.CreateCommand();
                    buildingsCommand.CommandText = @"SELECT NAME, LOCATION, ACTIVE FROM Buildings WHERE location LIKE '%'+ @Param2 +'%'";

                    buildingsCommand.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daBuildings = new SqlDataAdapter();
                    daBuildings.SelectCommand = buildingsCommand;
                    daBuildings.Fill(ds, "FoundBuilding");

                    foreach (DataRow row in ds.Tables["FoundBuilding"].Rows)
                    {
                        Ustanova u = new Ustanova();
                        u.NazivUstanove = (string)row["name"];
                        u.LokacijaUstanove = (string)row["location"];
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundBuildings.Add(u);
                    }
                }
            }
            else if (param1 == "active")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand buildingsCommand = connection.CreateCommand();
                    buildingsCommand.CommandText = @"SELECT NAME, LOCATION, ACTIVE FROM Buildings WHERE active LIKE '%'+ @Param2 +'%'";

                    buildingsCommand.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daBuildings = new SqlDataAdapter();
                    daBuildings.SelectCommand = buildingsCommand;
                    daBuildings.Fill(ds, "FoundBuilding");

                    foreach (DataRow row in ds.Tables["FoundBuilding"].Rows)
                    {
                        Ustanova u = new Ustanova();
                        u.NazivUstanove = (string)row["name"];
                        u.LokacijaUstanove = (string)row["location"];
                        u.Upotreba = (bool)row["active"];
                        ModelHelper.GetInstance().FoundBuildings.Add(u);
                    }
                }
            }

        }
        public static void SearchPeriodsBy(string param1, string param2)
        {
            if (param1 == "VremeZauzeca")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"SELECT VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE FROM Termini WHERE vremezauzeca LIKE '%'+ @Param2 +'%'";

                    command.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daPeriods = new SqlDataAdapter();
                    daPeriods.SelectCommand = command;
                    daPeriods.Fill(ds, "FoundPeriod");

                    foreach (DataRow row in ds.Tables["FoundPeriod"].Rows)
                    {
                        Termin t = new Termin();
                        t.VremeZauzeca = (string)row["VremeZauzeca"];
                        t.Datum = (string)row["Datum"];
                        TipNastave tipNastave;
                        Enum.TryParse((string)row["Type"], out tipNastave);
                        t.tipNastave = tipNastave;
                        t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                        t.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundPeriods.Add(t);
                    }
                }

            }
            else if (param1 == "Datum")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"SELECT VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE FROM Termini WHERE datum LIKE '%'+ @Param2 +'%'";

                    command.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daPeriods = new SqlDataAdapter();
                    daPeriods.SelectCommand = command;
                    daPeriods.Fill(ds, "FoundPeriod");

                    foreach (DataRow row in ds.Tables["FoundPeriod"].Rows)
                    {
                        Termin t = new Termin();
                        t.VremeZauzeca = (string)row["VremeZauzeca"];
                        t.Datum = (string)row["Datum"];
                        TipNastave tipNastave;
                        Enum.TryParse((string)row["Type"], out tipNastave);
                        t.tipNastave = tipNastave;
                        t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                        t.Active = (bool)row["active"];

                        ModelHelper.GetInstance().FoundPeriods.Add(t);
                    }
                }
            }
            else if (param1 == "Type")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"SELECT VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE FROM Termini WHERE type LIKE '%'+ @Param2 +'%'";

                    command.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daPeriods = new SqlDataAdapter();
                    daPeriods.SelectCommand = command;
                    daPeriods.Fill(ds, "FoundPeriod");

                    foreach (DataRow row in ds.Tables["FoundPeriod"].Rows)
                    {
                        Termin t = new Termin();
                        t.VremeZauzeca = (string)row["VremeZauzeca"];
                        t.Datum = (string)row["Datum"];
                        TipNastave tipNastave;
                        Enum.TryParse((string)row["Type"], out tipNastave);
                        t.tipNastave = tipNastave;
                        t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                        t.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundPeriods.Add(t);
                    }
                }
            }
            else if (param1 == "Active")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"SELECT VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE FROM Termini WHERE active LIKE '%'+ @Param2 +'%'";
                    command.Parameters.Add(new SqlParameter("@Param2", param2));
                    SqlDataAdapter daPeriods = new SqlDataAdapter();
                    daPeriods.SelectCommand = command;
                    daPeriods.Fill(ds, "FoundPeriod");

                    foreach (DataRow row in ds.Tables["FoundPeriod"].Rows)
                    {
                        Termin t = new Termin();
                        t.VremeZauzeca = (string)row["VremeZauzeca"];
                        t.Datum = (string)row["Datum"];
                        TipNastave tipNastave;
                        Enum.TryParse((string)row["Type"], out tipNastave);
                        t.tipNastave = tipNastave;
                        t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                        t.Active = (bool)row["Active"];
                        ModelHelper.GetInstance().FoundPeriods.Add(t);

                    }
                }
            }
            else if (param1 == "KorisnikId")
            {
                using (SqlConnection connection = new SqlConnection(CONNECTION_STRING))
                {
                    connection.Open();

                    DataSet ds = new DataSet();

                    SqlCommand command = connection.CreateCommand();
                    command.CommandText = @"SELECT VREMEZAUZECA, DATUM, TYPE, KORISNIKID, ACTIVE FROM Termini WHERE korisnikid LIKE '%'+ @Param2 +'%'";

                    command.Parameters.Add(new SqlParameter("@Param2", param2));

                    SqlDataAdapter daPeriods = new SqlDataAdapter();
                    daPeriods.SelectCommand = command;
                    daPeriods.Fill(ds, "FoundPeriod");

                    foreach (DataRow row in ds.Tables["FoundPeriod"].Rows)
                    {
                        Termin t = new Termin();
                        t.VremeZauzeca = (string)row["VremeZauzeca"];
                        t.Datum = (string)row["Datum"];
                        TipNastave tipNastave;
                        Enum.TryParse((string)row["Type"], out tipNastave);
                        t.tipNastave = tipNastave;
                        t.korisnik = ModelHelper.GetInstance().GetKorisnik((int)row["KorisnikId"]);
                        t.Active = (bool)row["active"];
                        ModelHelper.GetInstance().FoundPeriods.Add(t);
                    }
                }
            }
        }
    }
}
