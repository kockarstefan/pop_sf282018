﻿using Projekat2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for TerminiWindow.xaml
    /// </summary>
    public partial class TerminiWindow : Window
    {
        public Korisnik k { get; set; }
        public ICollectionView CollectionView;
        public ICollectionView SearchedCollectionView;
        public TerminiWindow(Korisnik k)
        {
            InitializeComponent();
            if (k.TipKorisnika.ToString() == "PROFESOR")
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().ProfessorsPeriods);
                TerminGrid.ItemsSource = CollectionView;
                TerminGrid.IsSynchronizedWithCurrentItem = true;

                SearchFilterCb.Items.Add("VremeZauzeca");
                SearchFilterCb.Items.Add("Datum");
                SearchFilterCb.Items.Add("Type");
                SearchFilterCb.Items.Add("Active");



            }
            else
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Termini);
                TerminGrid.ItemsSource = CollectionView;
                TerminGrid.IsSynchronizedWithCurrentItem = true;

                SearchFilterCb.Items.Add("VremeZauzeca");
                SearchFilterCb.Items.Add("KorisnikId");
                SearchFilterCb.Items.Add("Datum");
                SearchFilterCb.Items.Add("Type");
                SearchFilterCb.Items.Add("Active");
            }

        }

        private void AddTerminButton_Click(object sender, RoutedEventArgs e)
        {
            
            Termin noviTermin = new Termin();
            if (k.TipKorisnika.ToString() == "PROFESOR")
            {
                noviTermin.korisnik.Id = k.Id;
            }
            AddEditTerminWindow addEditTerminWindow = new AddEditTerminWindow(noviTermin);
            addEditTerminWindow.ShowDialog();

            CollectionView.Refresh();
        }

        private void EditTerminButton_Click(object sender, RoutedEventArgs e)
        {
            Termin SelektovaniTermin = (Termin)CollectionView.CurrentItem as Termin;
            if (SelektovaniTermin != null)
            {
                Termin old = (Termin)SelektovaniTermin.Clone();
                AddEditTerminWindow editTerminWindow = new AddEditTerminWindow(SelektovaniTermin, AddEditTerminWindow.TerminWindowState.Editing);
                if (editTerminWindow.ShowDialog() != true)
                {
                    int index = ModelHelper.GetInstance().Termini.IndexOf(SelektovaniTermin);
                    ModelHelper.GetInstance().Termini[index] = old;
                    CollectionView.Refresh();
                }
            }
        }

        private void DeleteTerminButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Termin SelektovaniTermin = CollectionView.CurrentItem as Termin;
                SelektovaniTermin.Active = false; 
                CollectionView.Refresh();
            }
        }

        private void SearchFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String param1 = (string)SearchFilterCb.SelectedItem;
            String param2 = SearchFilter.Text.ToString();
            if (param2 == "")
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Termini);
                ModelHelper.GetInstance().FoundPeriods.Clear();
                TerminGrid.ItemsSource = CollectionView;
                TerminGrid.IsSynchronizedWithCurrentItem = true;
            }
            else
            {
                ModelHelper.SearchPeriodsBy(param1, param2);
                SearchedCollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().FoundPeriods);
                TerminGrid.ItemsSource = SearchedCollectionView;
                TerminGrid.IsSynchronizedWithCurrentItem = true;
                CollectionView.Refresh();
            }
        }
    }

}
