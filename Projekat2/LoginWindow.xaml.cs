﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        Korisnik loggedInUser = new Korisnik();
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            String username = Username.Text;
            String password = UserPassword.Password;
           
            if (username == "" || password == "")
            {
                MessageBox.Show("Please fill out all the fields.");
                this.Hide();
                LoginWindow window = new LoginWindow();
                window.Show();
            }else if(ModelHelper.Login(username, password) == true)
            {
               loggedInUser = ModelHelper.GetInstance().GetKorisnikByKorIme(username);
                if (loggedInUser.TipKorisnika.ToString() == "PROFESOR")
                {
                    TerminiWindow window = new TerminiWindow(loggedInUser);
                    this.Hide();
                    window.Show();
                }
                else
                {
                    MainWindow window = new MainWindow(loggedInUser);
                    this.Hide();
                    window.Show();
                }
            }else
            {
                MessageBox.Show("Incorrect username or password.");
                LoginWindow window = new LoginWindow();
                this.Hide();
                window.Show();
            }
                
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void GuestButton_Click(object sender, RoutedEventArgs e)
        {
            string tipKorisnikaString = "GUEST";
            TipKorisnika tipKorisnika;
            Enum.TryParse(tipKorisnikaString, out tipKorisnika);
            loggedInUser.TipKorisnika = tipKorisnika;
            MainWindow window = new MainWindow(loggedInUser);
            this.Hide();
            window.Show();
        }
    }
}
