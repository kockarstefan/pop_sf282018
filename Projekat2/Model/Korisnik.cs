﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat2
{
    [Serializable]
    public class Korisnik
    {
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public int Id { get; set; }
        public string Email { get; set; }
        public string KorisnickoIme { get; set; }
        public string Lozinka { get; set; }
        public TipKorisnika TipKorisnika { get; set; }
        public bool Active { get; set; } = true;  
       
        public Korisnik() { }

        public Korisnik(Korisnik original)
        {
            Ime = original.Ime;
            Prezime = original.Prezime;
            Id = original.Id;
            Email = original.Email;
            KorisnickoIme = original.KorisnickoIme;
            Lozinka = original.Lozinka;
            TipKorisnika = original.TipKorisnika;
            Active = original.Active;
        }

        public object Clone()
        {
            Korisnik copy = new Korisnik();
            copy.Ime = Ime;
            copy.Prezime = Prezime;
            copy.Id = Id;
            copy.Email = Email;
            copy.KorisnickoIme = KorisnickoIme;
            copy.Lozinka = Lozinka;
            copy.TipKorisnika = TipKorisnika;
            copy.Active = Active;
            return copy;
        }
    }
}