﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat2.Model
{
    public class Ucionica
    {
        public int IdUcionice { get; set; }
        public int BrojUcionice { get; set; }
        public int BrojMesta { get; set; }
        public TipUcionice TipUcionice { get; set; }
        public bool Upotreba { get; set; } = true;

        public Ucionica() { }

        public Ucionica(Ucionica original)
        {
            IdUcionice = original.IdUcionice;
            BrojUcionice = original.BrojUcionice;
            BrojMesta = original.BrojMesta;
            TipUcionice = original.TipUcionice;
            Upotreba = original.Upotreba;
        }

        public object Clone()
        {
            Ucionica copy = new Ucionica();
            copy.IdUcionice = IdUcionice;
            copy.BrojUcionice = BrojUcionice;
            copy.BrojMesta = BrojMesta;
            copy.TipUcionice = TipUcionice;
            copy.Upotreba = Upotreba;
            return copy;
        }
    }
}
