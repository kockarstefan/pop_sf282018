﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat2
{
    public class Ustanova
    {
        public int Id { get; set; }
        public string NazivUstanove { get; set; }
        public string LokacijaUstanove { get; set; }
        public bool Upotreba { get; set; } = true;

        public Ustanova () {}

        public Ustanova(Ustanova original)
        {
            Id = original.Id;
            NazivUstanove = original.NazivUstanove;
            LokacijaUstanove = original.LokacijaUstanove;
            Upotreba = original.Upotreba;
        }
     
        public object Clone()
        {
            Ustanova copy = new Ustanova();
            copy.Id = Id;
            copy.NazivUstanove = NazivUstanove;
            copy.LokacijaUstanove = LokacijaUstanove;
            copy.Upotreba = Upotreba;
            return copy;
        }
    }
}
