﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat2.Model
{
    public class Termin
    {
        public enum TipNastave {Vezbe, Predavanja}

        public int Id { get; set; }
        public string VremeZauzeca { get; set; }
        public string Datum { get; set; }
        public TipNastave tipNastave { get; set; }
        public Korisnik korisnik { get; set; }
        public bool Active { get; set; }

        public Termin() { }

        public Termin(Termin original)
        {
            Id = original.Id;
            VremeZauzeca = original.VremeZauzeca;
            Datum = original.Datum;
            tipNastave = original.tipNastave;
            korisnik = original.korisnik;
            Active = original.Active;
        }

        public object Clone()
        {
            Termin copy = new Termin();
            copy.Id = Id;
            copy.VremeZauzeca = VremeZauzeca;
            copy.tipNastave = tipNastave;
            copy.korisnik = korisnik;
            copy.Active = Active;
            return copy;
        }


    }
}
