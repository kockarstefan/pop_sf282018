﻿using Projekat2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for ClassroomsWindow.xaml
    /// </summary>
    public partial class ClassroomsWindow : Window
    {
        public ICollectionView CollectionView;
        public ICollectionView SearchedCollectionView;
        public ClassroomsWindow()
        {
            InitializeComponent();
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ucionice);
            ClassroomsGrid.ItemsSource = CollectionView;
            ClassroomsGrid.IsSynchronizedWithCurrentItem = true;

            
            SearchFilterCb.Items.Add("classnumber");
            SearchFilterCb.Items.Add("numOfSeats");
            SearchFilterCb.Items.Add("classtype");
            SearchFilterCb.Items.Add("active");
        }

        

        private void AddClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            Ucionica novaUcionica = new Ucionica();
            AddEditClassroomWindow addEditClassroomWindow = new AddEditClassroomWindow(novaUcionica);
            addEditClassroomWindow.ShowDialog();
            CollectionView.Refresh();
            
        }

        private void EditClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            Ucionica SelektovanaUcionica = (Ucionica)CollectionView.CurrentItem as Ucionica;
            if (SelektovanaUcionica != null)
            {
                Ucionica old = (Ucionica)SelektovanaUcionica.Clone();
                AddEditClassroomWindow editClassroomWindow = new AddEditClassroomWindow(SelektovanaUcionica, AddEditClassroomWindow.ClassroomWindowState.Editing);
                if(editClassroomWindow.ShowDialog() != true)
                {
                    int index = ModelHelper.GetInstance().Ucionice.IndexOf(SelektovanaUcionica);
                    ModelHelper.GetInstance().Ucionice[index] = old;
                    CollectionView.Refresh();
                }
            }
        }

        private void DeleteClassroomButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Ucionica SelektovanaUcionica = CollectionView.CurrentItem as Ucionica;
                SelektovanaUcionica.Upotreba = false; ;
                CollectionView.Refresh();
            }
        }

        private void SearchFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String param1 = (string)SearchFilterCb.SelectedItem;
            String param2 = SearchFilter.Text.ToString();
            if (param2 == "")
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ucionice);
                ModelHelper.GetInstance().FoundClassrooms.Clear();
                ClassroomsGrid.ItemsSource = CollectionView;
                ClassroomsGrid.IsSynchronizedWithCurrentItem = true;
            }
            else
            {
                ModelHelper.SearchClassroomsBy(param1, param2);
                SearchedCollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().FoundClassrooms);
                ClassroomsGrid.ItemsSource = SearchedCollectionView;
                ClassroomsGrid.IsSynchronizedWithCurrentItem = true;
                CollectionView.Refresh();
            }
        }
    }
}
