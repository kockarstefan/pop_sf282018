﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            ModelHelper.GetInstance().InitKorisnici();
            ModelHelper.GetInstance().initUcionice();
            ModelHelper.GetInstance().InitUstanove();
            ModelHelper.GetInstance().initTermini();
            LoginWindow window = new LoginWindow();
            window.Show();
        }
    }
}
