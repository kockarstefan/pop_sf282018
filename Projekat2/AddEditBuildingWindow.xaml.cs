﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AddEditBuildingWindow : Window
    {
        public Ustanova Ustanova = new Ustanova();
        public enum BuildingWindowState { Addition, Editing}
        public ICollectionView CollectionView;
        private BuildingWindowState state;
        public AddEditBuildingWindow(Ustanova ustanova, BuildingWindowState buildingWindowState = BuildingWindowState.Addition)
        {
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ustanove);
            InitializeComponent();
            state = buildingWindowState;
            Ustanova = ustanova;
            DataContext = Ustanova;
        }

        private void SaveBuildingButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (state == BuildingWindowState.Addition)
            {
                Ustanova.Id = ModelHelper.GetInstance().Ustanove.Count() + 1;
                ModelHelper.GetInstance().Ustanove.Add(Ustanova);
                ModelHelper.AddUstanova(Ustanova);
            }
            else if (state == BuildingWindowState.Editing)
            {
                if (Ustanova.Id != 0)
                {
                    ModelHelper.EditUstanova(Ustanova);
                    CollectionView.Refresh();
                }

            }
            this.Close();
        }

        private void DeleteBuildingButton_Click(object sender, RoutedEventArgs e)
        {
            ModelHelper.DeleteUstanovu(Ustanova);
            ModelHelper.GetInstance().Ustanove.Remove(Ustanova);
        }
    }
}
