﻿using Projekat2.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Projekat2.Model.Termin;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for AddEditTerminWindow.xaml
    /// </summary>
    public partial class AddEditTerminWindow : Window
    {
        public Termin Termin = new Termin();
        public enum TerminWindowState { Addition, Editing }
        private TerminWindowState state;
        public ICollectionView CollectionView;
        public AddEditTerminWindow(Termin termin, TerminWindowState terminWindowState = TerminWindowState.Addition)
        {
            InitializeComponent();

            Termin = termin;
            state = terminWindowState;
            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Termini);
            TerminType.Items.Add("Vezbe");
            TerminType.Items.Add("Predavanja");
            DataContext = Termin;
        }

        private void SaveTerminButton_Click(object sender, RoutedEventArgs e)
        {

            this.DialogResult = true;
            if (state == TerminWindowState.Addition)
            {
                Termin.korisnik = ModelHelper.GetInstance().GetKorisnik(int.Parse(KorisnikId.Text));
                string terminType = (string)TerminType.SelectedItem;
                TipNastave tipNastave;
                Enum.TryParse(terminType, out tipNastave);
                Termin.tipNastave = tipNastave;
                ModelHelper.GetInstance().Termini.Add(Termin);
                ModelHelper.AddTermin(Termin);

            }
            else if (state == TerminWindowState.Editing)
            {
                if (Termin.Id != 0)
                {
                    string terminType = (string)TerminType.SelectedItem;
                    TipNastave tipNastave;
                    Enum.TryParse(terminType, out tipNastave);
                    Termin.tipNastave = tipNastave;
                    ModelHelper.EditTermin(Termin);
                    CollectionView.Refresh();
                }
            }
            this.Close();
        }

        private void DeleteTerminButton_Click(object sender, RoutedEventArgs e)
        {
            ModelHelper.DeletePeriod(Termin);
            ModelHelper.GetInstance().Termini.Remove(Termin);
        }
    }
}
