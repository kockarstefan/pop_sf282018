﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for BuildingsWindow.xaml
    /// </summary>
    public partial class BuildingsWindow : Window
    {
        public Korisnik k { get; set; }
        public ICollectionView CollectionView;
        public ICollectionView SearchedCollectionView;
        public BuildingsWindow(Korisnik k)
        {
            InitializeComponent();
            if (k.TipKorisnika.ToString() == "GUEST")
            {
                ADdBuildingButton.IsEnabled = false;
                EditBuildingButton.IsEnabled = false;
                DeleteBuildingButton.IsEnabled = false;
                SearchFilterCb.IsEnabled = false;
                SearchFilter.IsEnabled = false;

            }
            else
            {

                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ustanove);
                BuildingsGrid.ItemsSource = CollectionView;
                BuildingsGrid.IsSynchronizedWithCurrentItem = true;


                SearchFilterCb.Items.Add("name");
                SearchFilterCb.Items.Add("location");
                SearchFilterCb.Items.Add("active");
            }
        }
        


        
        private void AddBuildingButton_Click(object sender, RoutedEventArgs e)
        {
            Ustanova novaUstanova = new Ustanova();
            AddEditBuildingWindow addEditBuildingWindow = new AddEditBuildingWindow(novaUstanova);
            addEditBuildingWindow.ShowDialog();
            CollectionView.Refresh();
        }

        private void EditBuildingButton_Click(object sender, RoutedEventArgs e)
        {
            Ustanova SelektovanaUstanova = (Ustanova)CollectionView.CurrentItem as Ustanova;
            if (SelektovanaUstanova != null)
            {
                Ustanova old = (Ustanova)SelektovanaUstanova.Clone();
                AddEditBuildingWindow editBuildingWindow = new AddEditBuildingWindow(SelektovanaUstanova, AddEditBuildingWindow.BuildingWindowState.Editing);
                if (editBuildingWindow.ShowDialog() != true)
                {
                    int index = ModelHelper.GetInstance().Ustanove.IndexOf(SelektovanaUstanova);
                    ModelHelper.GetInstance().Ustanove[index] = old;
                    CollectionView.Refresh();
                }
            }
        }

        private void DeleteBuildingButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Da li ste sigurni?", "Potvrda",
                MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                Ustanova SelektovanaUstanova = CollectionView.CurrentItem as Ustanova;
                SelektovanaUstanova.Upotreba = false;
                CollectionView.Refresh();
            }
        }

        private void SearchFilter_TextChanged(object sender, TextChangedEventArgs e)
        {
            String param1 = (string)SearchFilterCb.SelectedItem;
            String param2 = SearchFilter.Text.ToString();
            if (param2 == "")
            {
                CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Ustanove);
                ModelHelper.GetInstance().FoundBuildings.Clear();
                BuildingsGrid.ItemsSource = CollectionView;
                BuildingsGrid.IsSynchronizedWithCurrentItem = true;
            }
            else
            {
                ModelHelper.SearchBuildingsBy(param1, param2);
                SearchedCollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().FoundBuildings);
                BuildingsGrid.ItemsSource = SearchedCollectionView;
                BuildingsGrid.IsSynchronizedWithCurrentItem = true;
                CollectionView.Refresh();
            }
        }
    }
}
