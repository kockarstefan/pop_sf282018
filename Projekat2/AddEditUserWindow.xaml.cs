﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class AddEditUserWindow : Window
    {

        public Korisnik Korisnik { get; set; }

        public enum UserWindowState { Addition, Editing};
        public ICollectionView CollectionView;
        private UserWindowState state;
        public AddEditUserWindow(Korisnik korisnik, UserWindowState userWindowState = UserWindowState.Addition)
        {
            InitializeComponent();

            CollectionView = CollectionViewSource.GetDefaultView(ModelHelper.GetInstance().Korisnici);
            Korisnik = korisnik;
            state = userWindowState;

            RolesComboBox.Items.Add("ADMIN");
            RolesComboBox.Items.Add("PROFESOR");
            RolesComboBox.Items.Add("ASISTENT");
            RolesComboBox.Items.Add("KORISNIK");
            DataContext = Korisnik;
        }

        private void SaveUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            if (state == UserWindowState.Addition)
            {
                string typeOfUser = (string)RolesComboBox.SelectedItem;
                TipKorisnika tipKorisnika;
                Enum.TryParse(typeOfUser, out tipKorisnika);
                Korisnik.TipKorisnika = tipKorisnika;
                ModelHelper.GetInstance().Korisnici.Add(Korisnik);
                ModelHelper.AddUser(Korisnik);
                CollectionView.Refresh();
            }
            else if (state == UserWindowState.Editing)
            {
                if (Korisnik.Id != 0)
                {
                    string typeOfUser = (string)RolesComboBox.SelectedItem;
                    TipKorisnika tipKorisnika;
                    Enum.TryParse(typeOfUser, out tipKorisnika);
                    Korisnik.TipKorisnika = tipKorisnika;
                    ModelHelper.EditUser(Korisnik);
                    CollectionView.Refresh();
                }
                 
            }
            this.Close();
           
        }

        private void DeleteUserButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
